@extends('layouts.app')

@section('content')
<div class="card ">
  <div class="card-body">
    <h2 class="card-title mb-3">Post From Test</h2>
    <p class="card-title text-muted">Author: {{$post->user->name}}</p>
    <p class="card-subtitle mb-3 text-muted mb-3">Created at: {{$post->created_at}}</p>
    <p class="card-text mb-3">Created at: {{$post->content}}</p>
    <div class="mt-3">
      <a href="/posts" class="card-link">View all posts</a>
    </div>
  </div>
</div>
@endsection
